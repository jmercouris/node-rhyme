var rhyme = require('rhyme');

var fs = require('fs'),
    readline = require('readline');

var rd = readline.createInterface({
    input: fs.createReadStream('source.txt'),
    output: process.stdout,
    terminal: false
});

rd.on('line', function(line) {
	var splitLine = line.split(' ');

	rhyme(function (r) {
		splitLine[0] = r.rhyme(splitLine[0])[10];
	    console.log(splitLine);
	});	
});

